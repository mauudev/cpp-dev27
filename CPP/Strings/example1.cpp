#include <iostream>
using namespace std;

size_t calc_length(const char *s)
{
    size_t i = 0;
    while(s[i] != '\0')
    {
        i++;
    }
    return i;
}

size_t calc_length_ap(const char *s)
{
    const char* orig = s;
    while(*s++);
    return s - orig - 1;
}

int main ()
{
    const char* q = "hello world";//esto no podemos modificar porq esta en el heap
    cout << q << "\n";

    // q[2] == *(q+2)
    char s[] = "hola mundo";//esto podemos modificar porq esta en el stack
    cout << s << "\n";

    s[0] ='c';
    cout << s << endl;
    // q[1] = 'e';
    // q[5] = 'w';
    cout << calc_length_ap(q) << endl;
    cout << calc_length_ap(s) << endl;

    const char *m = "perro";
    const char *n = m+2;
    cout << n << endl;
    n--;
    cout << n << endl;

    int x[] = {1,2,3,4,5};
    int *y = x+2;

    cout << *y << endl;
}   