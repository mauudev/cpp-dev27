/*
*** DANLING POINTERS ***
- Punteros a direcciones invalidas
- Punteros a direcciones de memoria ya liberada
- Punteros sin inicializar

*** PARA QUE SIRVEN LOS PUNTEROS ***
- Para modificar parametros de salida
*/

#include <iostream>
using namespace std;

int main()
{
    int m = 25;
    int *pm = &m;
    cout << pm << "\n";
    cout << *pm << "\n";
    *pm = 450;
    cout << m << "\n";
}