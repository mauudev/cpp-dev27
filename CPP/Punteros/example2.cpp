/*
*** DANLING POINTERS ***
- Punteros a direcciones invalidas
- Punteros a direcciones de memoria ya liberada
- Punteros sin inicializar

*** PARA QUE SIRVEN LOS PUNTEROS ***
- Para modificar parametros de salida
*/

#include <iostream>
using namespace std;

void q()
{
    int *s; int p;
    cout << s << "\n";
    cout << p << "\n";
}

int main()
{
    cout << "Esto es un fraude ! \n";
    cout << "Hello friday !" << endl;
    q();
}