/*
*** DANLING POINTERS ***
- Punteros a direcciones invalidas
- Punteros a direcciones de memoria ya liberada
- Punteros sin inicializar

*** PARA QUE SIRVEN LOS PUNTEROS ***
- Para modificar parametros de salida
*/

#include <iostream>
using namespace std;

int main()
{
    int *p = (int *)0x20181109;
    cout << p << "\n";
    cout << *p << "\n";
    return 0;
}
