/*
*** DANLING POINTERS ***
- Punteros a direcciones invalidas
- Punteros a direcciones de memoria ya liberada
- Punteros sin inicializar

*** PARA QUE SIRVEN LOS PUNTEROS ***
- Para modificar parametros de salida
*/

#include <iostream>
using namespace std;

void sum(int a, int b, int *c)
{
    *c = a + b;
}
int main()
{
    int a = 5, b = 8;
    int r;
    sum(a,b,&r);
    cout << r << endl;
}