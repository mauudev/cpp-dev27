#include <iostream>

using namespace std;

/*
HERENCIA

- Crear clases a partir de otras
- En c++ pueden heredar de una o mas clases
- Es la base de la reutilizacion de codigo
- Es la piedra fundamental del polimorfismo
- En c++ hay una jerarquia de clases predefinida
- Puede ser public, protected o private

POLIMORFISMO

- Cuando una misma instancia de una clase base, reaccionan de manera diferente ante un mismo mensaje
- Se implementa con sobreescritura
*/

class Vehicle
{
    size_t max_speed;

public:
    Vehicle(size_t ms):max_speed{ms}
    {

    }
    
    virtual ~Vehicle(){}

    virtual void print() const
    {
        cout << "MAX SPEED: " << max_speed << "\n";       
    }
};

class Airplane : public Vehicle
{
    size_t max_height;

public:
    Airplane(size_t ms, size_t mh):Vehicle{ms}, max_height{mh}
    {

    }

    void print() const override
    {
        Vehicle::print();
        cout << "MAX HEIGHT: " << max_height << "\n";
    } 
};

void show(const Vehicle &a)
{
    a.print();
}

int main()
{
    // Vehicle x{150};
    // Airplane y{500, 3000};
    // show(x);
    // show(y);

    Vehicle *z1 = new Vehicle{25};
    z1->print();

    Airplane *z2 = new Airplane{10,100};
    z2->print();

    Vehicle *z3 = new Airplane{80, 20};
    z3->print();

    delete z1;
    delete z2;
    delete z3;
    
}