#include <iostream>
#include <cstring>
#include <cstdlib>

using namespace std;

/* **** REFERENCIAS ****
- Son como "punteros" con restricciones
- Tienen sintaxis simplificado
- Sirven para hacer paso de parametros por referencia

*** RESTRICCIONES ***
- Se inicializan al crearse
- Son inmutables, una ref hace referencia a una var hasta q la muerte los separe
- No hay aritmetica de referencias
- No hay referencias nulas, no podemos comparar con nullptr
*/

void factorial(int n, int *r)
{
    int s = 1;
    for(int i = 2; i <= n; i++)
    {
        s *= i;
    }
    *r = s;
}

void factorial2(int n, int &r)
{
    r = 1;
    for(int i = 2; i <= n; i++)
    {
        r *= i;
    }
}

int main()
{
    int f;
    factorial(6, &f);
    cout << f << "\n";
    int g;
    factorial2(5, g);

    cout << "*******************" << "\n";
    int p = 25;
    int q = 40;
    int &rp = p;
    rp = q;
    rp ++;
    cout << p << "\n";
    cout << q << "\n";

    auto rq = q; 
    auto rs = rp;
    auto &rr = rq;
}