#include <iostream>

using namespace std;

enum Severity //C enum
{
    SV_HIGH;
    SV_MID;
    SV_LOW;
};

enum Height
{
    HG_HIGH;
    HG_SMALL;
};

enum class SeverityPlus//Scoped enum
{
    HIGH = 10;
    MID;
    LOW = 25;
};
