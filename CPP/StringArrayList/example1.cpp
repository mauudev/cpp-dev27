#include <iostream>
#include <cstring>
#include <cstdlib>

using namespace std;

struct StringArrayList{
    char** items;
    size_t n;

    public:
    size_t get_size(){return n;}
};

using SAL = StringArrayList;//ALIAS
typedef StringArrayList SAL2; //ALIAS (deprecated in c++11)

void init(SAL *s)
{
    s->items = (char**) malloc(0);//por defecto malloc retorna void* 
                                  //debemos castear a lo q estamos solicitando
    s->n = 0;
}

char* clone(const char* s)
{
    auto len = strlen(s);//strlen es muy lenta mejor si se la usa una vez por ciclo
    auto cs = new char[len + 1];
    // strcpy(cs,s);//muy lento pero como tenemos len mejor usar memcpy
    memcpy(cs,s,len+1);//es mas eficiente en maquinas de 32(copia de 2 en 2 bits) 
                       //y 64(copia de 4 en 4 bits)
    return cs;
}

void add(SAL *ss, const char* s)
{
    ss->items = (char**) realloc(ss->items, (ss->n+1)*sizeof(char*));
    ss->items[ss->n] = clone(s);
    ss->n++;
}

void print(const SAL *s)
{
    for(size_t i = 0; i < s->n; i++)
    {
        cout << s->items[i] << "\n";
    }
}

void release(SAL *s)
{
    for(size_t i = 0; i < s->n; i++)
    {
        delete [] s->items[i];//liberamos los char*
    }
    free(s->items);//liberamos el char**
}

int main()
{
    SAL ss;
    init(&ss);

    add(&ss, "hello");
    add(&ss, "world");
    add(&ss, "friends");
    add(&ss, "of");
    add(&ss, "c++");
    add(&ss, "hello");
    add(&ss, "hello");
    add(&ss, "hello");
    
    // int i = 0; 
    // while(i < 100000000000)
    // {
    //     add(&ss, "hello");
    //     i++;    
    // }
    // cout << ss.get_size() << endl;
}