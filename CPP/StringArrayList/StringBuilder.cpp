#include <iostream>
#include <cstring>
#include <cstdlib>
#include <stdio.h>

using namespace std;

struct StringBuilder 
{
    char* str;
    size_t len;

public:
    size_t get_size(){return len;}
};

using SB = StringBuilder;

void init(SB *sb)
{
    sb->str = (char*) malloc(0);
    sb->len = 0;
}

void add(SB *sb, const char *s)
{
    auto len = strlen(s);
    sb->str = (char*) realloc(sb->str, (sb->len+1)*sizeof(char*));
    memcpy(sb->str + sb->len, s, len + 1);
    sb->len = sb->len + len;
}

void add(SB *sb, int n)//usar: char aux[20] -> itoa(458, aux, 10)
{
    int digits = 0;
    int aux = n;
    while(aux != 0)
    {
        aux /= 10;
        digits ++;
    }
    char *s = (char *) malloc(digits); 
    sprintf(s, "%d",n);
    auto slen = strlen(s);
    sb->str = (char*) realloc(sb->str, (sb->len+1)*sizeof(char*));
    memcpy(sb->str + sb->len, s, slen + 1);
    sb->len += slen; 
}

void add_line(SB *sb, const char *s)
{
    auto len = strlen(s);
    sb->str = (char*) realloc(sb->str, (sb->len+1)*sizeof(char*));
    memcpy(sb->str + sb->len, s, len + 1);
    sb->len = sb->len + len;
}

size_t get_length(const SB *sb)
{
    return sb->len;
}

const char* get_string(const SB *sb)
{
    return sb->str;
}

void release(SB *sb)
{
    delete [] sb->str;
    cout << "Bye !" << "\n";
}

int main()
{
    SB sb;
    init(&sb);
    add(&sb, "hello");
    add(&sb, "hello2");
    add(&sb, "hello3");
    add_line(&sb, "\n");
    add(&sb, 123);
    add(&sb, 123456);
    cout << get_string(&sb) << "\n";
    cout << get_length(&sb) << "\n";
    release(&sb);
}