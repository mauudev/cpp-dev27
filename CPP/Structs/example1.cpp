#include <iostream>
#include <cstring>

using namespace std;

struct Persona
{
    char nombre[32];
    char apellido[32];
    size_t ci;
};

class Persona2
{
    public:
    char nombre[32];
    char apellido[32];
    size_t ci;
};

void init(Persona *p, const char* n, const char* a, size_t ci)
{
    // strcpy((*p).nombre,n);
    // strcpy((*p).apellido,a);
    // (*p).ci = ci;
    strcpy(p->nombre,n);
    strcpy(p->apellido,a);
    p->ci = ci;
}

void print(const Persona *p)
{
    cout << p->nombre << " " << p->apellido << " " << p->ci << "\n"; 
}

int main()
{
    Persona p;
    init(&p,"Ivan","Rios",1234567);
    print(&p);

    Persona *h = new Persona{};
    init(h, "Jose Maria", "Leyes",4566778);
    print(h);
    delete h;
}