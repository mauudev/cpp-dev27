#include <iostream>
using namespace std;

bool is_even(int n)
{
    return (n % 2) == 0;
}

void print_day(int d)
{
    switch(d)
    {
        case 0:
            cout << "Domingo \n";
            break;
        case 1:
            cout << "Lunes\n";
            break;
        case 2:
            cout << "Martes\n";
            break;
        case 3:
            cout << "Miercoles\n";
            break;
        default: 
            cout << "Invalid day ! \n";
            break;

    }

}

void count_down_recursive(int n)//provoca segmentation fault por ser recursivo
{
    cout << n << "\n";
    if(n == 0) return ;
    count_down_recursive(n-1);
}

void count_down(int n)
{
    while(n >= 0) cout << n-- << "\n";
}

void count (int a, int b)
{
    for(int i = a; i <= b; i++)
    {
        cout << i << "\n";
    }
}

void count(int a, int b, int ex)
{
    int i = a;
    // while(i <= b)
    // {
    //     if(i == ex)i++;
    //     continue;
    //     cout << i << "\n";
    // }
    for(int i = a; i <= b; i++)
    {
        if(i == ex) continue;
        cout << i << '\n';
    }
}

int main()
{
    cout << is_even(64) <<"\n";
    cout << is_even(63) <<"\n";
    print_day(0);
    // count_down(50000000);
    count(1,15);
    count(1,15,11);
}

//tarea
/*
void print_as_text(unsigned int n);

print_as_text(2481);

-> "Dos mil cuatroscientos y uno"
   "dieciocho"
   "ventiocho"
   145 : ciento cuarenta y cinco
   100 : cien
*/