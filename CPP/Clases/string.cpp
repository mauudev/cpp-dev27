#include <iostream>
#include <cstring>
#include <cstdlib>

using namespace std;

class String 
{
    size_t len;
    char *str;

public:
    String(const char* s): len{strlen(s)}//initialization list
    {
        str = (char*)malloc(len+1);
        memcpy(str, s, len+1);
    }

    String():String{""}
    {

    }

    String(const String &s):len{s.len},str{(char*)malloc(len+1)}
    {
        memcpy(str, s.str, len+1);
    }

    String& operator=(const String &s)
    {
        if(this == &s) return *this;
        this->~String();
        len = s.len;
        str = (char*) malloc(len+1);
        memcpy(str, s.str, len+1);
        return *this;
    }

    ~String()
    {
        free(str);
    }

    const char* c_str() const
    {
        return str;
    }

    String operator+(const String &s) const
    {
        String aux;
        auto nlen = len + s.len;
        aux.str = (char*) realloc(aux.str, nlen+1);
        memcpy(aux.str, str, len);
        memcpy(aux.str + len, s.str, s.len + 1);
        aux.len = nlen;
        return aux;
    }

    String& operator+=(const String &s)
    {
        auto nlen = len + s.len;
        str = (char*) realloc(str, nlen+1);
        memcpy(str + len, s.str, s.len + 1);
        len = nlen;
        return *this;
    }

    bool operator==(const String &s) const;
    bool operator!=(const String& s) const;
    void trim();    
    int index_of(const String &s) const; // ana -> zanahoria = 1
    String substring(size_t init, size_t len) const; // (1,3) = ana
    String substring(size_t init) const;

    //cout << String("zanahoria".substring(1,3).c_str() << endl; //ana
    //cout << String("coliflor").substring(4).c_str() << endl; //flor
    //String p = "batman"
    //int x = p.index_of("man"); // 3
    //x = p.index_of("men") // -1
};

class Person 
{
    String first_name;
    String last_name;
    int id;

public:
    Person(const String& fn, const String& ln, int i):first_name{fn},last_name{ln},id{i}
    {

    }

    void print() const 
    {
        cout << first_name.c_str() << " " << last_name.c_str() << " " << id << "\n";
    }
};

int main()
{
    // String s;
    // String s2{"hola"};
    // String s3 = "mundo";
    // String s4 = s2;
    // s = s3;

    // // cout << s4.c_str() << "\n";
    // // cout << s.c_str() << "\n";

    // auto hm = s4 + " " + s;
    // hm += "nativo";
    // cout << hm.c_str() << "\n";

    Person p1 {"Elon", "Musk", 123456};
    Person p2 {"Ma", "Cuma", 666};
    Person p3 = p1;
    p1 = Person {"Dick", "Grimes", 38777};
    p1.print();
    p2.print();
    p3.print();
}