#include <iostream>
#include <cstring>
#include <cstdlib>

using namespace std;


/*
- Son structs con todo privado por defecto

*** RAII ***
- Resource
- Acquisition
- Is
- Initialization

QUE SIGNIFICA USAR LA MEMORIA DEL STACK SIEMPRE CUANDO SE PUEDA
*/

class Curso
{
    char nombre [16];
    size_t alumnos;

    public:
    
    Curso(const char *n, size_t al)
    {
        strcpy(nombre, n);
        alumnos = al;
    }

    ~Curso()
    {
        cout << "Adios !" << "\n";
    }

    void mostrar() const //NO MODIFICA LAS VARIABLES DE LA CLASE
    {
        cout << nombre << " " << alumnos << "\n";
    }
};

int main()
{
    Curso c1{"dev27",18};
    c1.mostrar();
    
    Curso *c2 = new Curso{"dev28", 0};
    c2->mostrar();
    delete c2;
}