#include <iostream>
#include <cstring>
#include <cstdlib>

using namespace std;

class Carro
{
    char *desc;
    char id[10];

public:
    Carro(const char* des, const char* id)
    {
        auto len = strlen(des);
        desc = new char[len+1];
        memcpy(desc, des, len + 1);
        strcpy(this->id, id);
    }

    Carro(const Carro& c) //CONSTRUCTOR COPIA
    {
        strcpy(id, c.id);
        auto len = strlen(c.desc);
        desc = new char[len + 1];
        memcpy(desc, c.desc, len + 1);
    }

    ~Carro()
    {
        cout << "Bye carro !" << "\n";
        delete [] desc;
    }

    Carro& operator=(const Carro &c)
    {
        if(this == &c)
            return *this;
        this->~Carro();
        auto len = strlen(c.desc);
        desc = new char[len+1];
        memcpy(desc, c.desc, len+1);
        return *this;
    }

    const char* get_description() const
    {
        return desc;
    }

    const char* get_id() const
    {
        return id;
    }
};

int main()
{
//     auto c1 = Carro{"Peta 1976", "0123ABC"};

//     {
//     Carro c2{"Toyota Corolla", "GTK765"};
//     cout << c1.get_description() << "\n";
//     cout << c2.get_id() << "\n"; 
//     }
//     cout << "Hoy es lunes" << "\n";
    auto c1 = Carro{"Lamborghini Huracan 2019","3838FGG"};
    auto c2 = c1;//COPIA

    Carro c3{"BMW X6 2020","3847DFDF"};
    c3 = c2; //ASSIGNMENT
}