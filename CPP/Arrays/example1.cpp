/*
- Conjunto de elementos del mismo tipo
- Arrays tienen el tamano fijo
- Los elementos se acceden mediante un indice
*/

#include <iostream>
using namespace std;

int main()
{
    int q[5];//Este valor del tamano tiene q ser conocido en tiempo de compilacion ! no podemos
    //definirlo por pase de parametro
    q[0] = 10;
    q[1] = 5;
    q[2] = 8;
    q[3] = 4;
    q[4] = 9;
    for(int n : q)
    {
        cout << n << "\n";
    }

    int *r = q;
    for (int i = 0; i < 5; i++) cout << r[i] << "\n";
    q[5] = 29;
    cout << q[5] << "\n";
    for(int i = 0; i < 1000000; i++)
    {
        cout << q[i] << "\n";
    }
}